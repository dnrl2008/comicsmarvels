
#Aplicación
La  aplicación  funciona  con  5  componentes  fundamentales: 
- HTML 
- PHP5 
- BOOTSTRAP
- JQUERY
- WEBSERVICE 

El web service es suministrado por el cliente Marvels, durante el desarrollo de la aplicación se hizo uso de la herramienta git para el manejo de cambios de la aplicación. 

##Patron de Diseño
La aplicación consta de un modelo de datos, de información de presentación y una simulación de controlador.

###WEBSERVICE
La Documentación se puede encontrar en la siguiente dirección:
https://developer.marvel.com/documentation/generalinfo 

#####Componentes
Interfaz grafica: index.php
control de la petición:controller.php
Modelo de datos: serviceMarvelModel.php

####Repository
https://gitlab.com/dnrl2008/comicsmarvels

#####Requerimientos

PHP5
Jquery-3.4.1
BOOTSTRAP v4.3.1

