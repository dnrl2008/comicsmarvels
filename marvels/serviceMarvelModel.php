<?php 

class MarvelServiceM
{
	private $ts;
	private $privateKey;
	private $publicKey;
	private $hash;
	private $url;
	private $settings;

	function __construct(){
		$this->ts 		    =$this->prepareTs();
		$this->privateKey	="08bcacea72f9cc96f174e13eece8eb1c400032c8";
		$this->publicKey	="d87ff3156866e4faa160d1e4b0666ad7";
		$this->hash		    =$this->hash();
		$this->settings     =
		[
   			CURLOPT_CONNECTTIMEOUT => 10,
    		CURLOPT_RETURNTRANSFER => 1,
    		CURLOPT_TIMEOUT        => 60,
    		CURLOPT_POST           => false,
    		CURLOPT_HTTPHEADER     => ['Content-Type: application/json']
  		];

	}
	public function dataServiceMarvel($id){
		return $this->getHttp($id);
	}
	private function hash(){
		return md5($this->ts.$this->privateKey.$this->publicKey);
	}
	private function prepareTs(){
		$timestamp= new DateTime();
		return $timestamp->getTimestamp();
	}
	private function getHttp($id){
		$this->url="http://gateway.marvel.com/v1/public/characters/".$id."?ts=".$this->ts."&apikey=".$this->publicKey."&hash=".$this->hash;
	
		$ch= curl_init($this->url);
  		curl_setopt_array($ch, $this->settings);
		if (!$res = curl_exec($ch)) {
		   $res = ['error' => 'fail', 'msg' => curl_error($ch)];
		   return $res;
		}

		if (!$decoded = json_decode($res, true)) {
   			$res = ['error' =>'fail', 'msg' => json_last_error_msg()];
   			return $res;
  		}

  		if (!curl_errno($ch)) {
		  switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
		    case 200:  
		    	$res=['error'=>'ok','data'=>$decoded];
		    break;
		    case 404:  
		    	$res=['error'=>'fail','msg'=>'Personaje no Encontrado'];
		    break;
		    case 401:  
		    	$res=['error'=>'fail','msg'=>'Error de Hash o apikey Invalido'];
		    break;
		    default:
		    	$res=['error'=>'fail','msg'=>'Código HTTP inesperado: '.$http_code];
		  }
		}
  		curl_close($ch);	
		return $res;
	}
}

?>




