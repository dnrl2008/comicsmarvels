$(document).ready(function() {
	var isSendingAjax=false;
	$('#bt1').click(function(ev){
		ev.preventDefault();
		var id=$('#idPerson').val();
		$('.img1').css('display','none');
		$('.img3').css('display','none');
		if(!$.isNumeric(id)){
			msg('El ID debe ser Númerico');
			return;
		}

		if(isSendingAjax){
			return;
		}
		isSendingAjax=true;
		$.get('controller.php',
			{
				id: id
			},function(data){
				isSendingAjax=false;
				if(data.error=="ok"){
					$('.img2').css('display','none');
					$('.img1').css('display','block');
					$('.name').text(data.data.data.results[0]['name']);
					$('.descripcion').text(data.data.data.results[0]['description']);
					$.each(data.data.data.results[0]['comics']['items'],function(i,item){
						$('.comics .lista').append('<li>'+item.name+'</li>');
					});
				}else if(data.error='fail'){
					msg(data.msg);
				}
			},'json').fail(function(){
		});
	});

	$('#bt2').click(function(){
		$('.img1').css('display','none');
		$('.img2').css('display','block');
		$('#idPerson').val('')

	});
		
});

function msg(msg){
	$('.img2').css('display','none');
	$('.img3').css('display','block');
	$('.msg').text(msg);
	setTimeout(function(){ 
		$('.img3').css('display','none');
		$('.img2').css('display','block');
		$('.msg').text('');
	}, 3000);
}


