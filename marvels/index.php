
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Comics Marvel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="ccs/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="ccs/css.css">
</head>
<body>

<nav class="navbar navbar-inverse ">
	
  	<div class="container text-center">
	    <form method="get" class="form-inline" id="form-id">
	  		<div class="form-group">
	      		<label class="texth1">Ingrese ID personaje a buscar </label><br>
				<input type="text" class="form-control" id="idPerson" name="id" value="">
				<input id="bt1" type="submit" class="btn btn-success" value="Enviar" >
	    	</div>	
		</form> 
  	</div>
</nav>
<div class="jumbotron img3">
	<div class="row aler alert-danger">
		<span class="msg"></span>
	</div>
  
</div>

<div class="jumbotron img2">
  
</div>
<div class="jumbotron img1">
	<label>Name</label><br>
	<span class="name"></span><br><br>
	<label>Descripción</label><br>
	<span class="descripcion"></span><br><br>
	<label>#Comics</label><br>
	<span class="comics">
		<ul class="lista">

		</ul>
	</span>
	<input id="bt2" type="button" class="btn btn-success" value="Salir" >
  
</div>
<footer>
	
</footer>
  
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/js.js"></script>

</body>
</html>